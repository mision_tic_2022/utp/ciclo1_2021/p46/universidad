"""
Repositorio:
https://gitlab.com/mision_tic_2022/utp/ciclo1_2021/p46
"""

from controlador.controlador import Controlador

#Clase que me representa la inferfaz de usuario
class Ui:
    #Método constructor
    def __init__(self):
        self.controlador: Controlador = Controlador()
        self.menu()

    def menu(self):
        opc: int = 0
        while opc != -1:
            print("1 --> Crear estudiante")
            print("2 --> Matricular materia")
            print("3 --> Consultar estudiante")
            print("4 --> Guardar todos los cambios")
            print("-1 --> Salir")
            opc = int( input("Por favor ingrese una opción: ") )

            if opc == 1:
                nombre: str = input("Nombre del estudiante: ")
                apellido: str = input("Apellido del estudiante: ")
                cedula: str = input("Cédula del estudiante: ")
                genero: str = input("Género del estudiante: ")
                #crear estudiante
                self.controlador.crear_estudiante(nombre, apellido, cedula, genero)
            elif opc == 2:
                cedula_estudiante: str = input("Cedula del estudiante: ")
                nombre_materia: str = input("Nombre de la materia: ")
                self.controlador.matricular_materia(nombre_materia, cedula_estudiante)
            elif opc == 4:
                self.controlador.save()


"""
1 --> Crear estudiante
2 --> Consultar estudiante
"""
"""
https://gitlab.com/mision_tic_2022/utp/ciclo1_2021/p46
"""