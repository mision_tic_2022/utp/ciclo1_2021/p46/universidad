#Importa los elementos de tkinter
from tkinter import *
#Widgets
from tkinter.ttk import *

#Construir un objeto tipo ventana
window = Tk()
#Agregarle un título a la ventana
window.title("Mi primera ventana")
#Crear etiqueta para mostrar 'nombre:'
lbl_nombre = Label(window, text="Nombre: ")
entry_nombre = Entry(window, width=15)
lbl_apellido = Label(window, text="Apellido: ")
entry_apellido = Entry(window, width=15)

def guardar():
    print("click!")
    #Obtenemos los datos del campo
    print("Nombre: "+entry_nombre.get())
    print("Apellido: "+entry_apellido.get())
    #Eliminamos los datos del campo de texto
    #recibe como parametro el inicio y fin de la cadena
    entry_nombre.delete(0, 'end')
    entry_apellido.delete(0, 'end')
#Crear un boton
btn_guardar = Button(window, text="Guardar", command=guardar)
#----------Tabla--------
treeview = Treeview(window, columns=2)
treeview["columns"] = ["nombre", "apellido"]
treeview["show"] = "headings"
treeview.heading("nombre", text="Nombre")
treeview.heading("apellido", text="Apellido")
#Añadir elementos a la "tabla"
treeview.insert('', 0, 0, values=("andres", "quintero"))
treeview.insert('', 1, 1, values=("juan", "paredes"))
"""
Ubicar los elementos en la ventana
"""
#Indicar la posición que estará en la ventana
lbl_nombre.grid(column=0, row=0)
entry_nombre.grid(column=1, row=0)
lbl_apellido.grid(column=0, row=1)
entry_apellido.grid(column=1, row=1)
btn_guardar.grid(column=1, row=2)
treeview.grid(column=2, row=3)

"""
Propiedades de la ventana
"""
#Indicar el tamaño de la ventana en pixeles anchoxalto
window.geometry("650x300")
#mostrar la ventana
window.mainloop()