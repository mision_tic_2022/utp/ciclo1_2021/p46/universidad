
from modelo.estudiante import Estudiante

#Clase que me representa a una universidad
class Universidad:
    #Método constructor
    def __init__(self, nombre: str) -> None:
        #Atributos
        self.nombre = nombre
        self.estudiantes: list[Estudiante] = []
    
    #Método consultor
    def get_nombre(self)->str:
        return self.nombre
    
    def get_estudiantes(self)->list:
        return self.estudiantes
    
    #Método modificador
    def set_nombre(self, nombre: str):
        self.nombre = nombre
    
    def set_estudiantes(self, estudiantes: list):
        self.estudiantes = estudiantes
    
    #Acciones
    def matricular_estudiante(self, estudiante: Estudiante):
        self.estudiantes.append(estudiante)