#Clase que me representa una materia
class Materia:
    #Método constructor
    def __init__(self, nombre: str) -> None:
        #Atributos
        self.nombre: str = nombre
        self.nota: float = None
    
    #Métodos consultores / getters
    def get_nombre(self)->str:
        return self.nombre
    
    def get_nota(self)->float:
        return self.nota
    
    #Métodos modificadores / setters
    def set_nombre(self, nombre:str):
        self.nombre = nombre
    
    def set_nota(self, nota: float):
        self.nota = nota