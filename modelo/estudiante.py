

#Importa la clase Materia

#Clase que me representa a un estudiante
from modelo.materia import Materia


class Estudiante:
    #Método constructor
    def __init__(self, nombre: str, apellido: str, cedula: str, genero: str) -> None:
        self.nombre: str = nombre
        self.apellido: str = apellido
        self._cedula: str = cedula
        self.genero: str = genero
        self.materias: list[Materia] = []
    
    def __str__(self) -> str:
        str_estudiante: str = "Nombre: "+self.nombre+"\n"
        str_estudiante += "Apellido: "+self.apellido+"\n"
        str_estudiante += "Género: "+self.genero+"\n"
        str_estudiante += "Cédula: "+self._cedula+"\n"
        return str_estudiante

    #Métodos consultores
    def get_nombre(self)->str:
        return self.nombre
    
    def get_apellido(self)->str:
        return self.apellido
    
    def get_cedula(self)->str:
        return self._cedula

    def get_genero(self)->str:
        return self.genero
    
    def get_materias(self)->list:
        return self.materias
    
    #Métodos modificadores
    def set_nombre(self, nombre: str):
        self.nombre = nombre

    def set_apellido(self, apellido: str):
        self.apellido = apellido

    def set_genero(self, genero: str):
        self.genero = genero
    
    def set_materias(self):
        self.materias
    
    #Acciones de la clase
    def matricular_materia(self, nombre_materia: str):
        #Construimos un objeto de tipo Materia
        obj_materia: Materia = Materia(nombre_materia)
        self.materias.append(obj_materia)
    
    def cancelar_materia(self, nombre_materia: str):
        #Filtra la lista de objetos con el nombre de la materia a filtrar
        filtro_materia = list(filter(lambda m: m.get_nombre().lower() == nombre_materia.lower(), self.materias))
        #Elimina el objeto de la lista por su indice
        #i = self.materias.index(filtro_materia[0])
        #self.materias.pop(i)
        #Elimina el objeto de la lista
        self.materias.remove(filtro_materia[0])

"""
Código de la clase Estudiante:
https://paste.ofcode.org/54Lkp7q6dQuq5KHUUHPZs2
"""