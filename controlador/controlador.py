#Clase que me representa un controlador
from modelo.materia import Materia
from modelo.estudiante import Estudiante
from modelo.universidad import Universidad
import json

class Controlador:
    #Método constructor
    def __init__(self) -> None:
        self.universidad: Universidad = Universidad("UTP")
    
    #Métodos consultores
    def get_universidad(self)->Universidad:
        return self.universidad
    
    #Métodos modificadores
    def set_universidad(self, universidad: Universidad):
        self.universidad = universidad
    
    def crear_estudiante(self, nombre, apellido, cedula, genero)->Estudiante:
        #Crear objeto a partir de los datos del estudiante
        obj_estudiante: Estudiante = Estudiante(nombre, apellido, cedula, genero)
        self.universidad.matricular_estudiante(obj_estudiante)
        return obj_estudiante
    
    def matricular_materia(self, nombre_materia: str, cedula_estudiante: str):
        estudiante = self.buscar_estudiante(cedula_estudiante)
        estudiante.matricular_materia(nombre_materia)
    
    def buscar_estudiante(self, cedula: str)->Estudiante:
        #Filtramos la búsqueda del estudiante por medio de la cédula
        filtro_estudante = list(filter(lambda est: est.get_cedula() == cedula, self.universidad.get_estudiantes()))
        #Retornamos el objeto filtrado que se encuentra en la posición cero
        return filtro_estudante[0]

    #Guardar todos los cambios
    def save(self):
        diccionario_principal: dict = {}
        cont: int = 1
        for estudiante in self.universidad.get_estudiantes():
            diccionario_principal['0'+str(cont)] = {
                "nombre": estudiante.get_nombre(),
                "apellido": estudiante.get_apellido(),
                "cedula": estudiante.get_cedula(),
                "genero": estudiante.get_genero(),
                "materias": [ {"nombre": m.get_nombre(), "nota": m.get_nota()} for m in estudiante.get_materias() ]
            }
            cont += 1
        try:
            with open("modelo/bd.json", "w") as fichero:
                json.dump(diccionario_principal, fichero)
        except:
            print("Error al guardar los datos")


"""
Código del controlador:
https://paste.ofcode.org/y5m8aTCsuQyrLaMTGz4WyG
"""
"""
{
    '01': {
        "nombre": "",
        "apellido": "",
        ...,
        "materias": [{nombre: --, nota: --}]
    },
    ...
}
"""